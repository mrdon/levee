package com.atlassian.levee.runtime;

public interface MemoryUsagePolicy
{
    /**
     *  Return the maximum number of bytes the sandboxed code can allocate per entry point
     */
    long getMaxBytes();
}
