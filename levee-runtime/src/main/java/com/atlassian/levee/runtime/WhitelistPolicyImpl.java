package com.atlassian.levee.runtime;

import com.sun.servicetag.SystemEnvironment;

import java.util.Arrays;

public class WhitelistPolicyImpl implements WhitelistPolicy, LeveeProperties
{
    public boolean isWhitelisted(String className, String methodName, String methodSignature)
    {
        if (!getAllowedPackages().iterator().hasNext())
        {
            return true;
        }
        for (String pkg : getAllowedPackages())
        {
            if (className.startsWith(pkg))
            {
                return true;
            }
        }
        return false;
    }

    private Iterable<String> getAllowedPackages()
    {
        String[] packages = System.getProperty(LEVEE_WHITELIST_PACKAGES_PROPERTY, "").split(",");
        return Arrays.asList(packages);
    }
}
