package com.atlassian.levee.runtime;

public class MemoryUsageExceededException extends RuntimeException
{
    public MemoryUsageExceededException(String message)
    {
        super(message);
    }
}
