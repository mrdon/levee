package com.atlassian.levee.runtime;

public interface TimeoutPolicy
{
    /**
     * The maximum time this computation can run, in milliseconds
     * @return the maximum time this computation can run, in milliseconds
     */
    int getTimeoutMs();
}
