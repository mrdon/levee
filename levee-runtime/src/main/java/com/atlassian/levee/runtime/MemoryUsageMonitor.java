package com.atlassian.levee.runtime;

import com.atlassian.levee.sizeof.SizeOf;

import java.util.concurrent.atomic.AtomicLong;

public final class MemoryUsageMonitor
{
    private static MemoryUsagePolicy memoryUsagePolicy;

    private MemoryUsageMonitor()
    {
        throw new UnsupportedOperationException("Can't instantiate this class");
    }

    // Number of bytes remaining
    private static volatile ThreadLocal<Long> remainingBytes = new ThreadLocal<Long>()
    {
        @Override
        protected Long initialValue()
        {
            return 0L;
        }
    };

    public static void init()
    {
        memoryUsagePolicy = new MemoryUsagePolicyImpl();
        remainingBytes.set(memoryUsagePolicy.getMaxBytes());
    }

    public static void reset() {
        remainingBytes.set(0L);
    }

    public static void checkAllocation(long bytesToAllocate)
    {
        assert(bytesToAllocate >= 0);
        if (remainingBytes == null || memoryUsagePolicy.getMaxBytes() == 0)
        {
            return;
        }
        remainingBytes.set(remainingBytes.get() - bytesToAllocate);
        //System.out.println("remaining bytes: " + remainingBytes.get() + " thread: " + Thread.currentThread().getId());
        if (remainingBytes.get() <= 0)
        {
            throw new MemoryUsageExceededException("Memory usage exceeded " + memoryUsagePolicy.getMaxBytes() + " bytes " +
                "by " + remainingBytes.get() + " bytes with allocation of " + bytesToAllocate + " bytes");
        }
    }

    // Does a dynamic check of the size of an instance of this class, and increments remainingBytes by that amount
    public static void checkAllocation(Object obj)
    {
        if (obj != null)
        {
            checkAllocation(SizeOf.sizeof(obj));
        }
    }
}
