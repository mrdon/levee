package com.atlassian.levee.runtime;

public class TimeoutAndWhitelistMonitor
{
    private static ThreadLocal<Long> startTime = new ThreadLocal<Long>()
    {
        @Override
        protected Long initialValue()
        {
            return 0L;
        }
    };
    private static TimeoutPolicy timeoutPolicy;
    private static WhitelistPolicy whitelistPolicy;

    public static void init()
    {
        timeoutPolicy = new TimeoutPolicyImpl();
        whitelistPolicy = new WhitelistPolicyImpl();
        startTime.set(System.currentTimeMillis());
    }

    public static void checkTimeout()
    {
        if (timeoutPolicy.getTimeoutMs() == 0L)
        {
            return;
        }
        long currentTime = System.currentTimeMillis();
        if (currentTime - startTime.get() > timeoutPolicy.getTimeoutMs())
        {
            /*
            System.out.println("Timeout exn, exceeded: " + timeoutPolicy.getTimeoutMs() + " ms" );
            System.out.println("Timeout exn, time was: " + (currentTime - startTime.get()) + " ms" );
            */
            throw new TimeoutException();
        }
    }

    public static void checkTimeoutAndWhitelist(String className, String methodName, String methodSignature)
    {
        long currentTime = System.currentTimeMillis();
        if (whitelistPolicy == null) return;

        if (!whitelistPolicy.isWhitelisted(className, methodName, methodSignature))
        {
//            System.out.println("whitelist exn: " + className + " " + methodName);
            throw new WhitelistException();
        }
        if (timeoutPolicy.getTimeoutMs() == 0L)
        {
            return;
        }
        if (currentTime - startTime.get() > timeoutPolicy.getTimeoutMs())
        {
            /*
            System.out.println("timeout exn: " + className + " " + methodName);
            System.out.println("Timeout exn, exceeded: " + timeoutPolicy.getTimeoutMs() + " ms" );
            System.out.println("Timeout exn, time was: " + (currentTime - startTime.get()) + " ms" );
            */
            throw new TimeoutException();
        }
    }

}
