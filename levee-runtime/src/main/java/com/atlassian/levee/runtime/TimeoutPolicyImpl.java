package com.atlassian.levee.runtime;

public class TimeoutPolicyImpl implements TimeoutPolicy, LeveeProperties
{
    public TimeoutPolicyImpl()
    {
    }


    public int getTimeoutMs()
    {
        return timeoutMs();
    }


    private static int timeoutMs()
    {
       return Integer.getInteger(LEVEE_TIMEOUT_PROPERTY, DEFAULT_TIMEOUT_MS);
    }
}
