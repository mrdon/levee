package com.atlassian.levee.runtime;

public class MemoryUsagePolicyImpl implements MemoryUsagePolicy, LeveeProperties
{
    public long getMaxBytes()
    {
        return maxBytes();
    }

    private static int maxBytes()
    {
        return Integer.getInteger(LEVEE_MAX_BYTES_PROPERTY, DEFAULT_MAX_BYTES);
    }
}
