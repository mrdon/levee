package com.atlassian.levee.runtime;

public interface WhitelistPolicy
{
    boolean isWhitelisted(String className, String methodName, String methodSignature);
}
