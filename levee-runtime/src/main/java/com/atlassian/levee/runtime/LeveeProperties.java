package com.atlassian.levee.runtime;

public interface LeveeProperties
{
    public static final int DEFAULT_MAX_BYTES = 0;
    public static final int DEFAULT_TIMEOUT_MS = 0;

    public static final String LEVEE_TIMEOUT_PROPERTY = "com.atlassian.levee.runtime.timeout.ms";
    public static final String LEVEE_MAX_BYTES_PROPERTY = "com.atlassian.levee.runtime.max.bytes";
    public static final String LEVEE_WHITELIST_PACKAGES_PROPERTY = "com.atlassian.levee.runtime.whitelist.packages";
}
