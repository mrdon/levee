package com.atlassian.levee.instrumentor;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodAdapter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.ArrayList;
import java.util.HashMap;

public class LeveeMethodAdapter extends MethodAdapter
{
    // Used to determine whether we just saw a new, or if the INVOKESPECIAL
    // we're seeing is a superclass ctor
    boolean seenNew = false;
    boolean isEntryPoint = true;
    String name;

    public LeveeMethodAdapter(MethodVisitor methodVisitor, String name, boolean autoInit)
    {
        super(methodVisitor);
        this.name = name;
        // TODO : abstract this instead of hardcoding everything to be an entry point
        this.isEntryPoint = autoInit && !name.equals("<init>");
    }

    public void visitCode() {
        mv.visitCode();
        if (isEntryPoint)
        {
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/atlassian/levee/runtime/TimeoutAndWhitelistMonitor", "init", "()V");
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/atlassian/levee/runtime/MemoryUsageMonitor", "init", "()V");
        }
    }

    @Override
    public void visitJumpInsn(int opcode, Label label)
    {
        mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/atlassian/levee/runtime/TimeoutAndWhitelistMonitor", "checkTimeout", "()V");
        mv.visitJumpInsn(opcode, label);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc)
    {
        // Check the timeout and whitelist
        mv.visitLdcInsn(owner);
        mv.visitLdcInsn(name);
        mv.visitLdcInsn(desc);
        mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/atlassian/levee/runtime/TimeoutAndWhitelistMonitor", "checkTimeoutAndWhitelist", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");

        // Call the method
        mv.visitMethodInsn(opcode, owner, name, desc);

        // Update memory counters
        if (opcode == Opcodes.INVOKESPECIAL && "<init>".equals(name) && seenNew) {
            seenNew = false;
            // TODO : this assumes that the stack actually contains a copy of the ref, which is not necessarily true
            mv.visitInsn(Opcodes.DUP);
            mv.visitMethodInsn(Opcodes.INVOKESTATIC, "com/atlassian/levee/runtime/MemoryUsageMonitor", "checkAllocation", "(Ljava/lang/Object;)V");
        }
    }

    @Override
    public void visitTypeInsn(int i, String s)
    {
        mv.visitTypeInsn(i, s);
        if (i == Opcodes.NEW)
        {
            seenNew = true;
        }

        // TODO handle ANEWARRAY
    }
}
