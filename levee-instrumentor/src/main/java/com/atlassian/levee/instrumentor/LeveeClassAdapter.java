package com.atlassian.levee.instrumentor;


import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

public class LeveeClassAdapter extends ClassAdapter
{
    private final boolean autoInit;

    /**
     * Constructs a new {@link org.objectweb.asm.ClassAdapter} object.
     *
     * @param cv the class visitor to which this adapter must delegate calls.
     */
    public LeveeClassAdapter(ClassVisitor cv, boolean autoInit)
    {
        super(cv);
        this.autoInit = autoInit;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
        if (mv != null) {
            mv = new LeveeMethodAdapter(mv, name, autoInit);
        }
        return mv;
    }
}
