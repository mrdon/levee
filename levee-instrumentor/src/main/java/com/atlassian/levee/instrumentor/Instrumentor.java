package com.atlassian.levee.instrumentor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

public class Instrumentor
{
    public static void main(String... args)
    {
        if (args.length != 2) {
            System.out.println("Must pass in name of class file to instrument and name of destination file");
            System.exit(1);
        }

        File file = new File(args[0]);

        if (!file.getPath().endsWith(".class"))
        {
            System.out.println(file.getPath() + " doesn't look like a .class file");
            System.exit(1);
        }

        if (!file.exists())
        {
            System.out.println(file.getPath() + " does not exist");
            System.exit(1);
        }

        InputStream is = null;
        FileOutputStream out = null;
        try
        {
            is = new FileInputStream(file);
            out = new FileOutputStream(new File(args[1]));
            byte[] bytes = IOUtils.toByteArray(is);
            IOUtils.write(instrument(bytes, true), out);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
        finally
        {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(out);
        }
    }

    public static byte[] instrument(byte[] classfileBuffer, boolean autoInit)
    {
        ClassReader cr = new ClassReader(classfileBuffer);
        ClassWriter cw = new ClassWriter(cr, ClassWriter.COMPUTE_MAXS);
        LeveeClassAdapter lca = new LeveeClassAdapter(cw, autoInit);
        cr.accept(lca, 0);
        return cw.toByteArray();
    }
}
