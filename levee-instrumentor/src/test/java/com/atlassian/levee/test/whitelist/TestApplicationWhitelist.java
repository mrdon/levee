package com.atlassian.levee.test.whitelist;


import com.atlassian.levee.runtime.LeveeProperties;
import com.atlassian.levee.runtime.WhitelistException;
import com.atlassian.levee.test.SandboxedRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.URI;
import java.util.Date;

@RunWith(SandboxedRunner.class)
public class TestApplicationWhitelist
{
    @Before
    public void setup()
    {
        System.setProperty(LeveeProperties.LEVEE_WHITELIST_PACKAGES_PROPERTY, "com/atlassian/levee/test,java/lang,java/util");
    }

    @Test(expected = WhitelistException.class)
    public void callingMethodNotInWhitelistThrowsException()
    {
        URI.create("http://www.google.com");
    }

    @Test
    public void callingMethodInWhitelistedPackagesDoesNotThrowException()
    {
        java.util.Date date = new Date();
        date.getTime();
    }

    // TODO : test that non-whitelisted ctors don't work

}
