package com.atlassian.levee.test.timeout;

import com.atlassian.levee.runtime.LeveeProperties;
import com.atlassian.levee.runtime.TimeoutException;
import com.atlassian.levee.test.SandboxedRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SandboxedRunner.class)
public class TestApplicationTimeout
{
    @Before
    public void setup()
    {
        System.setProperty(LeveeProperties.LEVEE_TIMEOUT_PROPERTY, "300");
    }

    @Test(expected = TimeoutException.class)
    public void infiniteLoopThrowsTimeoutException()
    {
        int x = 0;
        while(true)
        {
            x++;
        }
    }
}
