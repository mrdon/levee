package com.atlassian.levee.test.memory;

import com.atlassian.levee.runtime.LeveeProperties;
import com.atlassian.levee.runtime.MemoryUsageExceededException;
import com.atlassian.levee.test.SandboxedRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

@RunWith(SandboxedRunner.class)
public class TestApplicationMemoryUsage
{
    @Before
    public void setup()
    {
        System.setProperty(LeveeProperties.LEVEE_MAX_BYTES_PROPERTY, "1000");
    }

    @Test(expected = MemoryUsageExceededException.class)
    public void exceedingMemoryUsageLimitThrowsMUEE()
    {
        for (int i = 0; i < 1000; i++)
        {
            new Date();
        }
    }
}
