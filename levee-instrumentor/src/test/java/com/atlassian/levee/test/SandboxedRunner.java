package com.atlassian.levee.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import static com.atlassian.levee.instrumentor.Instrumentor.instrument;

@Ignore
public class SandboxedRunner extends Runner
{
    private Runner runner;

    /**
     * Creates a BlockJUnit4ClassRunner to run {@code klass}
     *
     * @throws org.junit.runners.model.InitializationError
     *          if the test class is malformed.
     */
    public SandboxedRunner(final Class<?> clazz) throws InitializationError
    {
        final String oldName = clazz.getCanonicalName();
        final String newName = oldName + "$junitInst";
        final byte[] instrumentedClass = renameClass(oldName.replace('.','/'),
                                                     newName.replace('.','/'),
                                                     instrument(bytesForClass(clazz), true));

        Class<?> instrumented = loadClass(newName, instrumentedClass);
        this.runner = new BlockJUnit4ClassRunner(instrumented);
    }

    public static byte[] bytesForClass(Class<?> clazz)
    {
        String name = clazz.getName().replace('.','/') + ".class";
        InputStream is = clazz.getClassLoader().getResourceAsStream(name);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try
        {
            IOUtils.copy(is, baos);
            return baos.toByteArray();
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        finally {
            IOUtils.closeQuietly(is);
        }
        return null;
    }

    private byte[] renameClass(String oldName, String newName, byte[] bytes)
    {
        ClassReader cr = new ClassReader(bytes);
        ClassWriter cw = new ClassWriter(cr, ClassWriter.COMPUTE_MAXS);
        RenameClassAdapter rca = new RenameClassAdapter(cw, oldName, newName);
        cr.accept(rca, 0);
        return cw.toByteArray();
    }

    private Class loadClass(String className, byte[] b)
    {
        //override classDefine (as it is protected) and define the class.
        Class clazz = null;
        try
        {
            ClassLoader loader = ClassLoader.getSystemClassLoader();
            Class cls = Class.forName("java.lang.ClassLoader");
            java.lang.reflect.Method method =
                cls.getDeclaredMethod("defineClass", new Class[] { String.class, byte[].class, int.class, int.class });

            // protected method invocaton
            method.setAccessible(true);
            try
            {
                Object[] args = new Object[] { className, b, new Integer(0), new Integer(b.length)};
                clazz = (Class) method.invoke(loader, args);
            } finally
            {
                method.setAccessible(false);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
        return clazz;
    }

    @Override
    public Description getDescription()
    {
        return runner.getDescription();
    }

    @Override
    public void run(RunNotifier notifier)
    {
        runner.run(notifier);
    }
}
